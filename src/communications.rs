use serialport::SerialPort;
use std::io::ErrorKind;

const GRBL_BUFFER_SIZE: usize = 128;
const RX_BUFFER_SIZE: usize = 128;

#[derive(Debug, Default)]
pub struct Command {
    pub queued: Vec<u8>,
    pub realtime: Vec<u8>,
}

#[derive(Debug)]
pub struct ResponseLine {
    pub message: Vec<u8>,
    pub line: Vec<u8>,
}

#[derive(Debug)]
pub struct Responses {
    pub response_messages: Vec<ResponseLine>,
    pub push_messages: Vec<Vec<u8>>,
    pub queued: Vec<u8>,
    pub grbl_queued: Vec<u8>,
}

impl Responses {
    pub fn is_empty(&self) -> bool {
        self.response_messages.is_empty() && self.push_messages.is_empty()
    }
}

/// Handles talking to grbl
///  - Queues up gcode, and ensures grbl's internal buffer does not overflow
///  - Sends realtime commands as soon as possible, and may block on sending them
///  - Splits responses into lines.
///  - Removes `ok` responses and handles them.
///  - Dumps buffers on an `error` response.
pub struct Communications {
    serial: Box<dyn SerialPort>,

    /// Bytes that grbl will buffer, and need to make
    /// sure there is space in grbl's buffer
    tx_queued_buffer: Vec<u8>,

    /// Bytes that have been sent to grbl's queued buffer
    /// so we know how much space is left
    grbl_buffer: Vec<u8>,

    /// Bytes that have been received from grbl, but
    /// do not yet form a full line
    rx_buffer: Vec<u8>,
}

impl Communications {
    pub fn new(serial: Box<dyn SerialPort>) -> Communications {
        Communications {
            serial,
            tx_queued_buffer: Vec::new(),
            grbl_buffer: Vec::new(),
            rx_buffer: Vec::new(),
        }
    }

    pub fn update(&mut self, command: Command) -> Result<Responses, std::io::Error> {
        // Realtime commands get immediately picked of grbl's incoming buffer. They are often rather
        // time-sensitive, such as halting in case of emergency. This ensures that they are sent as
        // soon as possible.
        // https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#g-code-error-handling
        // https://github.com/gnea/grbl/wiki/Grbl-v1.1-Interface#streaming-protocol-character-counting-recommended-with-reservation
        self.serial.write_all(&command.realtime)?;

        self.tx_queued_buffer.extend_from_slice(&command.queued);

        // Grbl's buffer is only so big, so we need to make sure it doesn't overflow
        let buffer_remaining = GRBL_BUFFER_SIZE - self.grbl_buffer.len();

        // Find the end of the last line that fits in grbl's remaining buffer space
        let lines_length = self
            .tx_queued_buffer
            .iter()
            .take(buffer_remaining)
            .rposition(|&b| b == b'\n')
            .map(|l| l + 1)
            .unwrap_or(self.tx_queued_buffer.len());

        if lines_length > 0 && lines_length <= buffer_remaining {
            //println!(
            //"Sending: {:?}",
            //String::from_utf8_lossy(&self.tx_queued_buffer[0..lines_length])
            //);
            let written = match self.serial.write(&self.tx_queued_buffer[0..lines_length]) {
                Ok(n) => n,
                Err(e) => match e.kind() {
                    ErrorKind::Interrupted => 0,
                    _ => return Err(e),
                },
            };

            self.grbl_buffer
                .extend(self.tx_queued_buffer.drain(0..written));
        }

        let mut rx_buffer = [0; RX_BUFFER_SIZE];
        let read = match self.serial.read(&mut rx_buffer) {
            Ok(n) => n,
            Err(e) => match e.kind() {
                ErrorKind::Interrupted | ErrorKind::TimedOut => 0,
                _ => return Err(e),
            },
        };

        self.rx_buffer.extend_from_slice(&rx_buffer[0..read]);

        let mut responses = Responses {
            response_messages: Vec::new(),
            push_messages: Vec::new(),
            queued: self.tx_queued_buffer.clone(),
            grbl_queued: self.grbl_buffer.clone(),
        };

        while let Some(line_length) = self
            .rx_buffer
            .iter()
            .position(|&b| b == b'\n')
            .map(|l| l + 1)
        {
            let line = self
                .rx_buffer
                .drain(0..line_length)
                .skip_while(|b| b.is_ascii_whitespace())
                .collect::<Vec<_>>();
            //println!("recv: {:?}", String::from_utf8_lossy(&line));

            if line.starts_with(b"ok") {
                let remove_line_length = self
                    .grbl_buffer
                    .iter()
                    .position(|&b| b == b'\n')
                    .map(|l| l + 1)
                    .unwrap_or(self.grbl_buffer.len());

                let sent_line = self.grbl_buffer.drain(0..remove_line_length).collect();

                responses.response_messages.push(ResponseLine {
                    line: sent_line,
                    message: line,
                });
            } else if line.starts_with(b"error") {
                println!("{}", String::from_utf8_lossy(&line));

                // When if there is an error, grbl will dump the current line/block,
                // but keep executing things in its serial buffer. These commands could
                // have depended on what the errored command was, so may move unexpectedly.
                // To avoid this, we immediately send a soft reset, which causes grbl to
                // halt immediately.
                self.serial.write_all(&[0x18])?;

                let remove_line_length = self
                    .grbl_buffer
                    .iter()
                    .position(|&b| b == b'\n')
                    .map(|l| l + 1)
                    .unwrap_or(self.grbl_buffer.len());

                let sent_line = self.grbl_buffer.drain(0..remove_line_length).collect();

                responses.response_messages.push(ResponseLine {
                    line: sent_line,
                    message: line,
                });

                self.tx_queued_buffer.clear();
                self.grbl_buffer.clear();
            } else {
                responses.push_messages.push(line);
            }
        }

        Ok(responses)
    }
}

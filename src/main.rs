use crate::communications::{Command, Communications};
use crate::control::{Control, ControlMsg, Realtime, Status, Vector3};
use crate::sequence::{Gcode, ProbeToolLengthOffset, Program, System, SystemCommand};
use crate::stock::{ProbeTarget, ProbeType, Stock, StockShape};
use crossbeam::channel::Receiver;
use crossbeam::channel::Select;
use crossbeam::channel::Sender;
use std::fs::File;
use std::io::{stdin, stdout, BufRead, Read, Write};
use std::thread;
use std::time::Duration;

mod communications;
mod control;
mod gcode_macro;
mod sequence;
mod stock;

#[derive(Debug)]
enum CommunicationsError {
    SerialOpen(serialport::Error),
    Io(std::io::Error),
    Disconnected,
}

fn communications_thread(
    port: String,
    receiver: Receiver<ControlMsg>,
    sender: Sender<Result<Status, CommunicationsError>>,
) -> impl Fn() -> () {
    move || {
        let serial = match serialport::new(port.clone(), 115200).open() {
            Ok(serial) => serial,
            Err(e) => {
                sender
                    .send(Err(CommunicationsError::SerialOpen(e)))
                    .unwrap();
                return;
            }
        };

        let mut comms = Communications::new(serial);

        let mut control = Control::new();

        let mut command = Command::default();

        loop {
            let result = comms.update(command);

            match result {
                Ok(r) => {
                    let (status, new_command) = control.update(receiver.try_recv().ok(), r);
                    if let Some(status) = status {
                        sender.send(Ok(status)).unwrap();
                    }
                    command = new_command;
                }
                Err(e) => {
                    sender.send(Err(CommunicationsError::Io(e))).ok();
                    break;
                }
            }

            thread::sleep(Duration::from_millis(1));
        }
    }
}

fn stdin_thread(tx: Sender<String>) -> impl Fn() -> () {
    move || {
        for line in stdin().lock().lines() {
            tx.send(line.unwrap()).unwrap();
        }
    }
}

fn print_prompt(
    comms: &Option<(
        Sender<ControlMsg>,
        Receiver<Result<Status, CommunicationsError>>,
        Option<Status>,
    )>,
    message: Option<String>,
) {
    if let Some(msg) = message {
        println!("{}", msg);
    }
    if let Some((_, _, status)) = comms.as_ref() {
        if let Some(status) = status {
            println!(
                "Connected | {:?} | MPos: {}, {}, {} | WPos: {}, {}, {} | WCO: {}, {}, {}",
                status.state,
                status.position.x,
                status.position.y,
                status.position.z,
                status.position.x - status.wco.x,
                status.position.y - status.wco.y,
                status.position.z - status.wco.z,
                status.wco.x,
                status.wco.y,
                status.wco.z,
            );
            println!("Queued Gcode: ");
            println!("{}", String::from_utf8_lossy(&status.queued));
            println!("Grbl Queued Gcode: ");
            println!("{}", String::from_utf8_lossy(&status.grbl_queued));
        } else {
            println!("Connected");
        }
    } else {
        println!("Disconnected");
    }

    print!("> ");
    stdout().flush().ok();
}

fn main() {
    let mut comms: Option<(Sender<_>, Receiver<_>, Option<Status>)> = None;

    let mut stock = None;

    let (stdin_tx, stdin_rx) = crossbeam::channel::unbounded();

    thread::spawn(stdin_thread(stdin_tx));

    print_prompt(&comms, None);

    loop {
        let mut select = Select::new();

        select.recv(&stdin_rx);

        if let Some((_, comms_rx, _)) = comms.as_ref() {
            select.recv(comms_rx);
        }

        let selected = select.select();

        match selected.index() {
            0 => {
                let line = selected.recv(&stdin_rx).unwrap();
                let mut args = line.split_whitespace();
                let message = match args.next() {
                    Some("connect") => match args.next() {
                        Some(port) => {
                            let (tx_sender, tx_receiver) = crossbeam::channel::unbounded();
                            let (rx_sender, rx_receiver) = crossbeam::channel::unbounded();

                            thread::spawn(communications_thread(
                                port.to_string(),
                                tx_receiver,
                                rx_sender,
                            ));

                            comms = Some((tx_sender, rx_receiver, None));
                            None
                        }
                        None => Some("No port".to_string()),
                    },
                    Some("disconnect") => {
                        comms = None;
                        None
                    }
                    Some("stock") => match args.next() {
                        Some("rectangular") => {
                            if let [Ok(x), Ok(y), Ok(z)] = &(args
                                .take(3)
                                .map(|a| a.parse())
                                .collect::<Vec<Result<f32, _>>>())[..]
                            {
                                stock = Some(Stock {
                                    shape: StockShape::RectangularPrism {
                                        width: *x,
                                        depth: *y,
                                        height: *z,
                                    },
                                    probe_targets: Vec::new(),
                                });

                                None
                            } else {
                                Some("Missing x, y, or z".to_string())
                            }
                        }
                        Some("show") => Some(format!("{:#?}", stock)),
                        _ => Some("Unknown stock shape".to_string()),
                    },
                    Some("probe_target") => {
                        if let Some(stock) = stock.as_mut() {
                            match args.next() {
                                Some("clear") => {
                                    stock.probe_targets.clear();
                                    None
                                }
                                Some("add") => {
                                    if let (
                                        Some(x),
                                        Some(y),
                                        Some(z),
                                        Some(direction),
                                        Some(distance),
                                        Some(expected),
                                    ) = (
                                        args.next().and_then(|a| a.parse().ok()),
                                        args.next().and_then(|a| a.parse().ok()),
                                        args.next().and_then(|a| a.parse().ok()),
                                        args.next().and_then(|a| a.parse().ok()),
                                        args.next().and_then(|a| a.parse().ok()),
                                        args.next().and_then(|a| a.parse().ok()),
                                    ) {
                                        let target = ProbeTarget {
                                            probe_type: ProbeType::Single,
                                            start: Vector3 { x, y, z },
                                            direction,
                                            distance,
                                            relative: expected,
                                            absolute: None,
                                        };

                                        stock.probe_targets.push(target);

                                        None
                                    } else {
                                        Some("Mising arguments".to_string())
                                    }
                                }
                                _ => Some("Unknown probe target command".to_string()),
                            }
                        } else {
                            Some("No stock defined".to_string())
                        }
                    }
                    Some("b") => {
                        if let Some((comms_tx, _, _)) = comms.as_ref() {
                            let mut gcode_bytes = args.fold(String::new(), |mut gcode, arg| {
                                gcode.push_str(arg);
                                gcode.push(' ');
                                gcode
                            });
                            gcode_bytes.push('\n');

                            let gcode = gcode::parse(&gcode_bytes).collect();

                            comms_tx
                                .send(ControlMsg::Sequence(Box::new(Gcode(gcode))))
                                .unwrap();
                            None
                        } else {
                            Some("Not connected".to_string())
                        }
                    }
                    Some("hold") => {
                        if let Some((comms_tx, _, _)) = comms.as_ref() {
                            comms_tx.send(ControlMsg::Realtime(Realtime::Hold)).unwrap();
                            None
                        } else {
                            Some("Not connected".to_string())
                        }
                    }
                    Some("resume") => {
                        if let Some((comms_tx, _, _)) = comms.as_ref() {
                            comms_tx
                                .send(ControlMsg::Realtime(Realtime::Resume))
                                .unwrap();
                            None
                        } else {
                            Some("Not connected".to_string())
                        }
                    }
                    Some("stop") => {
                        if let Some((comms_tx, _, _)) = comms.as_ref() {
                            comms_tx.send(ControlMsg::Realtime(Realtime::Stop)).unwrap();
                            None
                        } else {
                            Some("Not connected".to_string())
                        }
                    }
                    Some("file") => {
                        if let (Some(path), Some((comms_tx, _, _))) = (args.next(), comms.as_ref())
                        {
                            match File::open(path) {
                                Ok(mut file) => {
                                    let mut gcode_bytes = String::new();
                                    file.read_to_string(&mut gcode_bytes).unwrap();

                                    let gcode = gcode::parse(&gcode_bytes).collect();

                                    comms_tx
                                        .send(ControlMsg::Sequence(Box::new(Gcode(gcode))))
                                        .unwrap();
                                    None
                                }
                                Err(e) => Some(format!("Could not open file: {:?}", e)),
                            }
                        } else {
                            Some("No path or not connected".to_string())
                        }
                    }
                    Some("program") => {
                        if let (
                            Some(path),
                            Some(safe_z),
                            Some(probe_feedrate),
                            Some(stock),
                            Some((comms_tx, _, _)),
                        ) = (
                            args.next(),
                            args.next().and_then(|a| a.parse().ok()),
                            args.next().and_then(|a| a.parse().ok()),
                            stock.as_ref(),
                            comms.as_ref(),
                        ) {
                            match File::open(path) {
                                Ok(mut file) => {
                                    let mut gcode_bytes = String::new();
                                    file.read_to_string(&mut gcode_bytes).unwrap();

                                    let gcode = gcode::parse(&gcode_bytes).collect();

                                    comms_tx
                                        .send(ControlMsg::Sequence(Box::new(Program {
                                            safe_z,
                                            probe_feedrate,
                                            gcode,
                                            stock: stock.clone(),
                                        })))
                                        .unwrap();
                                    None
                                }
                                Err(e) => Some(format!("Could not open file: {:?}", e)),
                            }
                        } else {
                            Some("No path, no stock, or not connected".to_string())
                        }
                    }

                    Some("home") => {
                        if let Some((comms_tx, _, _)) = comms.as_ref() {
                            comms_tx
                                .send(ControlMsg::Sequence(Box::new(System(SystemCommand::Home))))
                                .unwrap();
                            None
                        } else {
                            Some("Not connected".to_string())
                        }
                    }

                    Some("unlock") => {
                        if let Some((comms_tx, _, _)) = comms.as_ref() {
                            comms_tx
                                .send(ControlMsg::Sequence(Box::new(System(
                                    SystemCommand::Unlock,
                                ))))
                                .unwrap();
                            None
                        } else {
                            Some("Not connected".to_string())
                        }
                    }

                    Some("probe") => {
                        if let Some((comms_tx, _, _)) = comms.as_ref() {
                            comms_tx
                                .send(ControlMsg::Sequence(Box::new(ProbeToolLengthOffset {
                                    start: Vector3 {
                                        x: 10.0,
                                        y: 20.0,
                                        z: -30.0,
                                    },
                                    end: Vector3 {
                                        x: 10.0,
                                        y: 20.0,
                                        z: -40.0,
                                    },
                                    feedrate: 30.0,
                                })))
                                .unwrap();
                            None
                        } else {
                            Some("Not connected".to_string())
                        }
                    }
                    Some(cmd) => Some(format!("Unknown command: {}", cmd)),
                    None => None,
                };

                print_prompt(&comms, message);
            }
            1 => {
                let result = selected.recv(
                    &comms
                        .as_ref()
                        .expect("Got a message from a non-existent comms thread")
                        .1,
                );

                match result {
                    Ok(Ok(status)) => {
                        if let Some(comms) = comms.as_mut() {
                            comms.2 = Some(status)
                        }
                    }
                    Ok(Err(e)) => print_prompt(&comms, Some(format!("Got an error: {:?}", e))),
                    Err(_) => {
                        print_prompt(&comms, Some("Disconnected".to_string()));
                        comms = None;
                    }
                }
            }
            _ => unreachable!(),
        }
    }
}

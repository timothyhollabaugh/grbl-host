use crate::communications::{Command, Responses};
use crate::sequence::{SequenceExecute, SequenceHandle, SequenceReceive, SequenceSend};
use crate::stock::{Stock, StockShape};
use crossbeam::channel::{Receiver, Sender};
use std::time::{Duration, Instant};

const STATUS_UPDATE_INTERVAL: Duration = Duration::from_millis(200);

pub enum Realtime {
    Hold = b'!' as isize,
    Resume = b'~' as isize,
    Stop = 0x18,
}

pub enum ControlMsg {
    Sequence(Box<dyn SequenceExecute>),
    Realtime(Realtime),
}

#[derive(Debug, Copy, Clone)]
pub enum State {
    Idle,
    Run,
    HoldComplete,
    HoldInProgress,
    Jog,
    Alarm,
    DoorClosed,
    DoorOpen,
    DoorOpenInProgress,
    DoorClosedResuming,
    Check,
    Home,
    Sleep,
}

#[derive(Debug, Copy, Clone, Default)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector3 {
    pub fn parse(bytes: &[u8]) -> Result<Vector3, ()> {
        let mut fields = bytes.split(|b| b == &b',');

        Ok(Vector3 {
            x: fields
                .next()
                .and_then(|b| String::from_utf8(b.to_vec()).ok())
                .and_then(|b| b.parse().ok())
                .ok_or(())?,
            y: fields
                .next()
                .and_then(|b| String::from_utf8(b.to_vec()).ok())
                .and_then(|b| b.parse().ok())
                .ok_or(())?,
            z: fields
                .next()
                .and_then(|b| String::from_utf8(b.to_vec()).ok())
                .and_then(|b| b.parse().ok())
                .ok_or(())?,
        })
    }
}

pub enum StatusPosition {
    MPos(Vector3),
    WPos(Vector3),
}

#[derive(Debug)]
pub struct Status {
    pub state: State,
    pub position: Vector3,
    pub wco: Vector3,
    pub queued: Vec<u8>,
    pub grbl_queued: Vec<u8>,
}

pub struct Control {
    /// The last time a status report was sent
    last_status_time: Instant,

    /// Last reported work coordinate offset
    wco: Vector3,

    /// Last reported position
    position: Option<Vector3>,

    /// Last reported status
    state: Option<State>,

    sequence_thread_handle: Option<(Sender<SequenceReceive>, Receiver<SequenceSend>)>,
}

impl Control {
    pub fn new() -> Control {
        Control {
            last_status_time: Instant::now(),
            wco: Default::default(),
            position: None,
            state: None,
            sequence_thread_handle: None,
        }
    }

    pub fn update(
        &mut self,
        msg: Option<ControlMsg>,
        responses: Responses,
    ) -> (Option<Status>, Command) {
        let (new_sequence, new_realtime) = match msg {
            Some(ControlMsg::Sequence(sequence)) => (Some(sequence), None),
            Some(ControlMsg::Realtime(realtime)) => (None, Some(realtime)),
            None => (None, None),
        };

        for msg in responses.push_messages {
            if msg.starts_with(b"<") && msg.ends_with(b">\r\n") {
                let msg = &msg[1..msg.len() - 3];
                let fields = msg.split(|b| b == &b'|');

                let mut state = None;
                let mut wco = None;
                let mut position = None;

                for field in fields {
                    let mut args = field.split(|b| b == &b':');
                    match args.next() {
                        Some(b"Idle") => state = Some(State::Idle),
                        Some(b"Run") => state = Some(State::Run),
                        Some(b"Hold") => match args.next() {
                            Some(b"0") => state = Some(State::HoldComplete),
                            Some(b"1") => state = Some(State::HoldInProgress),
                            _ => {}
                        },
                        Some(b"Jog") => state = Some(State::Jog),
                        Some(b"Alarm") => state = Some(State::Alarm),
                        Some(b"Door") => match args.next() {
                            Some(b"0") => state = Some(State::DoorClosed),
                            Some(b"1") => state = Some(State::DoorOpen),
                            Some(b"2") => state = Some(State::DoorOpenInProgress),
                            Some(b"3") => state = Some(State::DoorClosedResuming),
                            _ => {}
                        },
                        Some(b"Check") => state = Some(State::Check),
                        Some(b"Home") => state = Some(State::Home),
                        Some(b"Sleep") => state = Some(State::Sleep),
                        Some(b"MPos") => {
                            position = args
                                .next()
                                .and_then(|b| Vector3::parse(b).ok())
                                .map(StatusPosition::MPos)
                        }
                        Some(b"WPos") => {
                            position = args
                                .next()
                                .and_then(|b| Vector3::parse(b).ok())
                                .map(StatusPosition::WPos)
                        }
                        Some(b"WCO") => wco = args.next().and_then(|b| Vector3::parse(b).ok()),
                        _ => {}
                    }
                }

                if let Some(wco) = wco {
                    self.wco = wco;
                }

                if let Some(position) = position {
                    match position {
                        StatusPosition::MPos(p) => self.position = Some(p),
                        StatusPosition::WPos(p) => {
                            self.position = Some(Vector3 {
                                x: p.x + self.wco.x,
                                y: p.y + self.wco.y,
                                z: p.z + self.wco.z,
                            })
                        }
                    }
                }

                if let Some(state) = state {
                    self.state = Some(state);
                }
            } else if let Some((sender, _)) = self.sequence_thread_handle.as_ref() {
                println!("{}", String::from_utf8_lossy(&msg));
                sender.send(SequenceReceive::Push(msg)).unwrap();
            }
        }

        if let (None, Some(mut sequence)) = (self.sequence_thread_handle.as_ref(), new_sequence) {
            let (tx_sender, tx_receiver) = crossbeam::channel::unbounded();
            let (rx_sender, rx_receiver) = crossbeam::channel::unbounded();

            std::thread::spawn(move || {
                let handle = SequenceHandle {
                    receiver: tx_receiver,
                    sender: rx_sender,
                };

                sequence.sequence_execute(&handle);

                handle.sender.send(SequenceSend::Done);
            });

            self.sequence_thread_handle = Some((tx_sender, rx_receiver));
        }

        let realtime = if let Some(realtime) = new_realtime {
            vec![realtime as u8]
        } else {
            Vec::new()
        };

        let mut command = Command {
            queued: Vec::new(),
            realtime,
        };

        if let Some((sender, receiver)) = self.sequence_thread_handle.as_ref() {
            match receiver.try_recv() {
                Ok(SequenceSend::Gcode(gcode)) => {
                    command = Command {
                        queued: gcode.into_iter().fold(Vec::new(), |mut bytes, gcode| {
                            bytes.extend_from_slice(gcode.to_string().as_bytes());
                            bytes.push(b'\n');
                            bytes
                        }),
                        realtime: Vec::new(),
                    };
                }
                Ok(SequenceSend::System(system)) => {
                    command = Command {
                        queued: system.to_string().into_bytes(),
                        realtime: Vec::new(),
                    };
                }
                Ok(SequenceSend::Done) => {
                    sender.send(SequenceReceive::Stop);
                    self.sequence_thread_handle = None
                }
                Err(_) => {}
            }
        }

        if !command.queued.is_empty() {
            println!("{}", String::from_utf8_lossy(&command.queued));
        }

        if Instant::now() - self.last_status_time > STATUS_UPDATE_INTERVAL {
            command.realtime.push(b'?');
            self.last_status_time = Instant::now();
        }

        let status = match (self.state, self.position) {
            (Some(state), Some(position)) => Some(Status {
                state,
                position,
                wco: self.wco,
                queued: responses.queued,
                grbl_queued: responses.grbl_queued,
            }),
            _ => None,
        };

        (status, command)
    }
}

use crate::control::Vector3;
use gcode::GCode;
use std::str::FromStr;

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum ProbeAxis {
    X,
    Y,
    Z,
}

impl FromStr for ProbeAxis {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "x" => Ok(ProbeAxis::X),
            "y" => Ok(ProbeAxis::Y),
            "z" => Ok(ProbeAxis::Z),
            _ => Err(()),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum ProbeType {
    Single,
    Midpoint,
}

/// A single probing
#[derive(Copy, Clone, Debug)]
pub struct ProbeTarget {
    pub probe_type: ProbeType,

    /// The start point of the probe
    pub start: Vector3,

    /// The direction that the probe moves.
    /// To move in the opposite direction,
    /// negate the `distance`
    pub direction: ProbeAxis,

    /// The distance to probe
    pub distance: f32,

    /// The position along the probe
    /// direction relative to the
    /// pre-transformed gcode that the
    /// probe contacts
    pub relative: f32,

    /// The position along the pobe
    /// direction absolute to the
    /// machine that the probe
    /// contacts
    pub absolute: Option<f32>,
}

#[derive(Copy, Clone, Debug)]
pub enum StockShape {
    RectangularPrism { width: f32, depth: f32, height: f32 },
}

#[derive(Clone, Debug)]
pub struct Stock {
    pub shape: StockShape,
    pub probe_targets: Vec<ProbeTarget>,
}

impl Stock {
    pub fn transform(&self, gcode: Vec<GCode>) -> Vec<GCode> {
        let x_offset = self
            .probe_targets
            .iter()
            .find_map(|target| {
                if let (ProbeAxis::X, Some(actual)) = (target.direction, target.absolute) {
                    Some(actual - target.relative)
                } else {
                    None
                }
            })
            .unwrap_or(0.0);

        let y_offset = self
            .probe_targets
            .iter()
            .find_map(|target| {
                if let (ProbeAxis::Y, Some(actual)) = (target.direction, target.absolute) {
                    Some(actual - target.relative)
                } else {
                    None
                }
            })
            .unwrap_or(0.0);

        let z_offset = self
            .probe_targets
            .iter()
            .find_map(|target| {
                if let (ProbeAxis::Z, Some(actual)) = (target.direction, target.absolute) {
                    Some(actual - target.relative)
                } else {
                    None
                }
            })
            .unwrap_or(0.0);

        gcode
            .into_iter()
            .map(|gcode| {
                let mnemonic = gcode.mnemonic();
                let major_number = gcode.major_number();
                let minor_number = gcode.minor_number();
                let span = gcode.span();
                let mut arguments = gcode.arguments().to_vec();

                for argument in arguments.iter_mut() {
                    match argument.letter {
                        'X' => argument.value += x_offset,
                        'Y' => argument.value += y_offset,
                        'Z' => argument.value += z_offset,
                        _ => {}
                    }
                }

                let mut new_gcode = GCode::new(
                    mnemonic,
                    major_number as f32 + (minor_number as f32 / 10.0),
                    span,
                );

                new_gcode.extend(arguments);

                new_gcode
            })
            .collect()
    }
}

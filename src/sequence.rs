use crate::control::Vector3;
use crate::gcode;
use crate::stock::{ProbeAxis, ProbeTarget, ProbeType, Stock};
use crossbeam::channel::{Receiver, RecvError, SendError, Sender};
use gcode::{GCode, Mnemonic, Span, Word};
use std::borrow::Cow;
use std::error::Error;
use std::fmt::{Display, Formatter};

#[derive(Copy, Clone)]
pub enum SystemCommand {
    Unlock,
    Home,
}

impl Display for SystemCommand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            &SystemCommand::Unlock => f.write_str("$X\n"),
            &SystemCommand::Home => f.write_str("$H\n"),
        }
    }
}

pub enum SequenceSend {
    Gcode(Vec<gcode::GCode>),
    System(SystemCommand),
    Done,
}

pub enum SequenceReceive {
    Push(Vec<u8>),
    Stop,
}

pub struct SequenceHandle {
    pub sender: Sender<SequenceSend>,
    pub receiver: Receiver<SequenceReceive>,
}

pub enum SequenceError {
    Aborted,
    Parse,
    Other(Box<dyn Error>),
}

impl From<Box<dyn Error>> for SequenceError {
    fn from(error: Box<dyn Error>) -> Self {
        SequenceError::Other(error)
    }
}

impl<T> From<SendError<T>> for SequenceError
where
    T: 'static + Send,
{
    fn from(error: SendError<T>) -> Self {
        SequenceError::Other(Box::new(error))
    }
}

impl From<RecvError> for SequenceError {
    fn from(error: RecvError) -> Self {
        SequenceError::Other(Box::new(error))
    }
}

/// Gets run in a separate thread, and can send an receive messages from
/// the controller.
pub trait Sequence {
    type Output;

    fn name(&self) -> Cow<str>;

    fn execute(&self, handle: &SequenceHandle) -> Result<Self::Output, SequenceError>;
}

/// A version of the Sequence trait that can be made into a trait object.
/// This is actually used by the controller, and is auto implemented for
/// anything that implements Sequence.
pub trait SequenceExecute: Send {
    fn name(&self) -> Cow<str>;

    fn sequence_execute(&self, handle: &SequenceHandle) -> Result<(), SequenceError>;
}

impl<T> SequenceExecute for T
where
    T: Sequence + Send,
{
    fn name(&self) -> Cow<str> {
        Sequence::name(self)
    }

    fn sequence_execute(&self, handle: &SequenceHandle) -> Result<(), SequenceError> {
        Sequence::execute(self, handle).map(|_| ())
    }
}

pub struct System(pub SystemCommand);

impl Sequence for System {
    type Output = ();

    fn name(&self) -> Cow<str> {
        "System".into()
    }

    fn execute(&self, handle: &SequenceHandle) -> Result<(), SequenceError> {
        Ok(handle.sender.send(SequenceSend::System(self.0))?)
    }
}

pub struct Gcode(pub Vec<gcode::GCode>);

impl Sequence for Gcode {
    type Output = ();

    fn name(&self) -> Cow<str> {
        "Gcode".into()
    }

    fn execute(&self, handle: &SequenceHandle) -> Result<(), SequenceError> {
        Ok(handle.sender.send(SequenceSend::Gcode(self.0.clone()))?)
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum ProbeStop {
    OnClose,
    OnOpen,
}

pub struct Probe {
    pub x: Option<f32>,
    pub y: Option<f32>,
    pub z: Option<f32>,
    pub stop: ProbeStop,
    pub error_on_failure: bool,
    pub feedrate: f32,
}

impl Sequence for Probe {
    type Output = (Vector3, bool);

    fn name(&self) -> Cow<str> {
        "Probe".into()
    }

    fn execute(&self, handle: &SequenceHandle) -> Result<Self::Output, SequenceError> {
        let gcode_number = match (self.stop, self.error_on_failure) {
            (ProbeStop::OnClose, true) => 38.2,
            (ProbeStop::OnClose, false) => 38.3,
            (ProbeStop::OnOpen, true) => 38.4,
            (ProbeStop::OnOpen, false) => 38.5,
        };

        let mut gcode = gcode::GCode::new(
            gcode::Mnemonic::General,
            gcode_number,
            gcode::Span::PLACEHOLDER,
        );

        if let Some(x) = self.x {
            gcode.push_argument(gcode::Word::new('X', x, Span::PLACEHOLDER));
        }

        if let Some(y) = self.y {
            gcode.push_argument(gcode::Word::new('Y', y, Span::PLACEHOLDER));
        }

        if let Some(z) = self.z {
            gcode.push_argument(gcode::Word::new('Z', z, Span::PLACEHOLDER));
        }

        gcode.push_argument(Word::new('F', self.feedrate, Span::PLACEHOLDER));

        Gcode(vec![gcode]).execute(handle)?;

        loop {
            match handle.receiver.recv()? {
                SequenceReceive::Stop => return Err(SequenceError::Aborted),
                SequenceReceive::Push(bytes) => {
                    if bytes.starts_with(b"[PRB:") {
                        let content = &bytes[1..bytes.len() - 1];
                        let mut args = content.split(|b| b == &b':').skip(1);

                        let position = Vector3::parse(args.next().ok_or(SequenceError::Parse)?)
                            .map_err(|_| SequenceError::Parse)?;
                        let success = args.next().ok_or(SequenceError::Parse)? == b"1";

                        return Ok((position, success));
                    }
                }
            }
        }
    }
}

pub struct ProbeToolLengthOffset {
    pub start: Vector3,
    pub end: Vector3,
    pub feedrate: f32,
}

impl Sequence for ProbeToolLengthOffset {
    type Output = f32;

    fn name(&self) -> Cow<str> {
        "Probe Tool Length Offset".into()
    }

    fn execute(&self, handle: &SequenceHandle) -> Result<Self::Output, SequenceError> {
        Gcode(vec![GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER)
            .with_argument(Word::new(
                'Z',
                self.start.z,
                Span::PLACEHOLDER,
            ))])
        .execute(handle)?;

        Gcode(vec![GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER)
            .with_argument(Word::new('X', self.start.x, Span::PLACEHOLDER))
            .with_argument(Word::new(
                'Y',
                self.start.y,
                Span::PLACEHOLDER,
            ))])
        .execute(handle)?;

        let (position, _) = Probe {
            x: Some(self.end.x),
            y: Some(self.end.y),
            z: Some(self.end.z),
            stop: ProbeStop::OnClose,
            error_on_failure: true,
            feedrate: self.feedrate,
        }
        .execute(handle)?;

        let tool_length = position.z - self.end.z;

        Gcode(vec![GCode::new(Mnemonic::General, 43.1, Span::PLACEHOLDER)
            .with_argument(Word::new(
                'Z',
                tool_length,
                Span::PLACEHOLDER,
            ))])
        .execute(handle)?;

        Ok(tool_length)
    }
}

pub struct ProbeTargets {
    pub targets: Vec<ProbeTarget>,
    pub safe_z: f32,
    pub feedrate: f32,
}

impl Sequence for ProbeTargets {
    type Output = Vec<ProbeTarget>;

    fn name(&self) -> Cow<str> {
        "Probe Targets".into()
    }

    fn execute(&self, handle: &SequenceHandle) -> Result<Self::Output, SequenceError> {
        let mut targets = self.targets.clone();

        for target in targets
            .iter_mut()
            .filter(|target| target.absolute.is_none())
        {
            match target.probe_type {
                ProbeType::Single => {
                    Gcode(vec![
                        gcode!(G0 Z{self.safe_z}),
                        gcode!(G0 X{target.start.x} Y{target.start.y}),
                        gcode!(G0 Z{target.start.z}),
                    ])
                    .execute(handle)?;

                    let (x, y, z) = match target.direction {
                        ProbeAxis::X => (Some(target.start.x + target.distance), None, None),
                        ProbeAxis::Y => (None, Some(target.start.y + target.distance), None),
                        ProbeAxis::Z => (None, None, Some(target.start.z + target.distance)),
                    };

                    let (result, _) = Probe {
                        x,
                        y,
                        z,
                        error_on_failure: true,
                        stop: ProbeStop::OnClose,
                        feedrate: self.feedrate,
                    }
                    .execute(handle)?;

                    target.absolute = match target.direction {
                        ProbeAxis::X => Some(result.x),
                        ProbeAxis::Y => Some(result.y),
                        ProbeAxis::Z => Some(result.z),
                    };

                    Gcode(vec![gcode!(G0 X{target.start.x} Y{target.start.y})]).execute(handle)?;
                }
                ProbeType::Midpoint => {
                    Gcode(vec![
                        gcode!(G0 Z{self.safe_z}),
                        gcode!(G0 X{target.start.x} Y{target.start.y}),
                        gcode!(G0 Z{target.start.z}),
                    ])
                    .execute(handle)?;

                    let (x, y, z) = match target.direction {
                        ProbeAxis::X => (Some(target.start.x + target.distance), None, None),
                        ProbeAxis::Y => (None, Some(target.start.y + target.distance), None),
                        ProbeAxis::Z => (None, None, Some(target.start.z + target.distance)),
                    };

                    let (result, _) = Probe {
                        x,
                        y,
                        z,
                        error_on_failure: true,
                        stop: ProbeStop::OnClose,
                        feedrate: self.feedrate,
                    }
                    .execute(handle)?;

                    Gcode(vec![gcode!(G0 X{target.start.x} Y{target.start.y})]).execute(handle)?;

                    let (x, y, z) = match target.direction {
                        ProbeAxis::X => (Some(target.start.x - target.distance), None, None),
                        ProbeAxis::Y => (None, Some(target.start.y - target.distance), None),
                        ProbeAxis::Z => (None, None, Some(target.start.z - target.distance)),
                    };

                    let (result2, _) = Probe {
                        x,
                        y,
                        z,
                        error_on_failure: true,
                        stop: ProbeStop::OnClose,
                        feedrate: self.feedrate,
                    }
                    .execute(handle)?;

                    target.absolute = match target.direction {
                        ProbeAxis::X => Some((result.x + result2.x) / 2.0),
                        ProbeAxis::Y => Some((result.y + result2.y) / 2.0),
                        ProbeAxis::Z => Some((result.z + result2.z) / 2.0),
                    };

                    Gcode(vec![gcode!(G0 X{target.start.x} Y{target.start.y})]).execute(handle)?;
                }
            }
        }

        Ok(targets)
    }
}

pub struct Program {
    pub stock: Stock,
    pub gcode: Vec<GCode>,
    pub probe_feedrate: f32,
    pub safe_z: f32,
}

impl Sequence for Program {
    type Output = ();

    fn name(&self) -> Cow<str> {
        "Program".into()
    }

    fn execute(&self, handle: &SequenceHandle) -> Result<Self::Output, SequenceError> {
        let mut stock = self.stock.clone();

        let targets = ProbeTargets {
            feedrate: self.probe_feedrate,
            safe_z: self.safe_z,
            targets: self.stock.probe_targets.clone(),
        }
        .execute(handle)?;

        Gcode(vec![gcode!(G0 Z{self.safe_z})]).execute(handle)?;

        stock.probe_targets = targets;

        let gcode = stock.transform(self.gcode.clone());

        Gcode(gcode).execute(handle)?;

        Gcode(vec![gcode!(G0 Z{self.safe_z})]).execute(handle)?;

        Ok(())
    }
}

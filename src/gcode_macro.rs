#[macro_export]
macro_rules! gcode {
    ( $n:ident $( $a:ident )* ) => {
        {
            let g = stringify!($n).chars().nth(0).unwrap();
            let n = stringify!($n).chars().skip(1).collect::<::std::string::String>().parse().unwrap();

            let mut gcode = ::gcode::GCode::new(
                ::gcode::Mnemonic::for_letter(g).unwrap(),
                n,
                ::gcode::Span::PLACEHOLDER,
            );

            $(
                let l = stringify!($a).chars().nth(0).unwrap();
                let v = stringify!($a).chars().skip(1).collect::<::std::string::String>().parse().unwrap();
                gcode.push_argument(
                    Word::new(
                        l,
                        v,
                        ::gcode::Span::PLACEHOLDER,
                    )
                ).unwrap();
            )*

            gcode
        }
    };
    ( $n:ident $($l:block$v:block)* ) => {
        {
            let g = stringify!($n).chars().nth(0).unwrap();
            let n = stringify!($n).chars().skip(1).collect::<::std::string::String>().parse().unwrap();

            ::gcode::GCode::new(
                ::gcode::Mnemonic::for_letter(g).unwrap(),
                n,
                ::gcode::Span::PLACEHOLDER,
            )

            $(
                .with_argument(::gcode::Word::new(
                    $l,
                    $v as f32,
                    ::gcode::Span::PLACEHOLDER,
                ))
            )*
        }
    };
    ( $n:ident $($l:ident$v:block)* ) => {
        {
            let g = stringify!($n).chars().nth(0).unwrap();
            let n = stringify!($n).chars().skip(1).collect::<::std::string::String>().parse().unwrap();

            ::gcode::GCode::new(
                ::gcode::Mnemonic::for_letter(g).unwrap(),
                n,
                ::gcode::Span::PLACEHOLDER,
            )

            $(
                .with_argument(::gcode::Word::new(
                    stringify!($l).chars().nth(0).unwrap(),
                    $v as f32,
                    ::gcode::Span::PLACEHOLDER,
                ))
            )*
        }
    };
    ( G$n:block $($l:block$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::General,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                $l,
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };
    ( M$n:block $($l:block$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::Miscellaneous,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                $l,
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };
    ( O$n:block $($l:block$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::ProgramNumber,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                $l,
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };
    ( T$n:block $($l:block$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::ToolChange,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                $l,
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };

    ( G$n:block $($l:ident$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::General,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                stringify!($l).chars().nth(0).unwrap(),
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };
    ( M$n:block $($l:ident$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::Miscellaneous,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                stringify!($l).chars().nth(0).unwrap(),
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };
    ( O$n:block $($l:ident$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::ProgramNumber,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                stringify!($l).chars().nth(0).unwrap(),
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };
    ( T$n:block $($l:ident$v:block)* ) => {
        ::gcode::GCode::new(
            ::gcode::Mnemonic::ToolChange,
            ($n) as f32,
            ::gcode::Span::PLACEHOLDER,
        )
        $(
            .with_argument(::gcode::Word::new(
                stringify!($l).chars().nth(0).unwrap(),
                $v as f32,
                ::gcode::Span::PLACEHOLDER,
            ))
        )*
    };
}

mod test {
    use gcode::{GCode, Mnemonic, Span, Word};

    #[test]
    fn test_gcode() {
        assert_eq!(
            gcode!(G{0}),
            GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER)
        );
    }

    #[test]
    fn test_mcode() {
        assert_eq!(
            gcode!(M{0}),
            GCode::new(Mnemonic::Miscellaneous, 0.0, Span::PLACEHOLDER)
        );
    }

    #[test]
    fn test_ocode() {
        assert_eq!(
            gcode!(O{0}),
            GCode::new(Mnemonic::ProgramNumber, 0.0, Span::PLACEHOLDER)
        );
    }

    #[test]
    fn test_tcode() {
        assert_eq!(
            gcode!(T{0}),
            GCode::new(Mnemonic::ToolChange, 0.0, Span::PLACEHOLDER)
        );
    }

    #[test]
    fn test_gcode5() {
        assert_eq!(
            gcode!(G{0} X{10}),
            GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        );
    }

    #[test]
    fn test_mcode5() {
        assert_eq!(
            gcode!(M{0} X{10}),
            GCode::new(Mnemonic::Miscellaneous, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        );
    }

    #[test]
    fn test_ocode5() {
        assert_eq!(
            gcode!(O{0} X{10}),
            GCode::new(Mnemonic::ProgramNumber, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        );
    }

    #[test]
    fn test_tcode5() {
        assert_eq!(
            gcode!(T{0} X{10}),
            GCode::new(Mnemonic::ToolChange, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        );
    }

    #[test]
    fn test_gcode_var_number() {
        let number = 1;
        assert_eq!(
            gcode!(G { number }),
            GCode::new(Mnemonic::General, 1.0, Span::PLACEHOLDER)
        )
    }

    #[test]
    fn test_gcode_arguments() {
        assert_eq!(
            gcode!(G{0} {'X'}{10}),
            GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        )
    }

    #[test]
    fn test_gcode2() {
        assert_eq!(
            gcode!(G0 X10),
            GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        )
    }

    #[test]
    #[should_panic]
    fn test_gcode2_fail() {
        gcode!(H0 X10);
    }

    #[test]
    fn test_gcode3() {
        assert_eq!(
            gcode!(G0 {'X'}{10}),
            GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        )
    }

    #[test]
    fn test_gcode4() {
        assert_eq!(
            gcode!(G0 X{10}),
            GCode::new(Mnemonic::General, 0.0, Span::PLACEHOLDER).with_argument(Word::new(
                'X',
                10.0,
                Span::PLACEHOLDER
            ))
        )
    }
}
